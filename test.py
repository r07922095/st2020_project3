# -*- coding: utf-8 -*
import os  

# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')
	os.system('sleep 1')

# 1. [Content] Side Bar Text
def test_checkSidebar():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('查看商品分類') != -1
	assert xmlString.find('查訂單/退訂退款') != -1
	assert xmlString.find('追蹤/買過/看過清單') != -1
	assert xmlString.find('智慧標籤') != -1
	assert xmlString.find('PChome 旅遊') != -1
	assert xmlString.find('線上手機回收') != -1
	assert xmlString.find('給24h購物APP評分') != -1
	os.system('sleep 1')




# 2. [Screenshot] Side Bar Text
def test_screenshotSideBar():
	os.system('adb shell screencap -p /sdcard/screen.png && adb pull /sdcard/screen.png ./siderBarText.png')
	os.system('sleep 1')

# 3. [Context] Categories
def test_checkCategories():
	os.system('adb shell input tap 200 1900')
	os.system('sleep 1')
	os.system('adb shell input swipe 200 1000 200 100')	
	os.system('sleep 1')
	os.system('adb shell input tap 1000 250')
	os.system('sleep 1')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./categories.xml')
	f = open('categories.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('精選') != -1
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('通訊') != -1
	assert xmlString.find('數位') != -1
	assert xmlString.find('家電') != -1
	assert xmlString.find('日用') != -1
	assert xmlString.find('食品') != -1
	assert xmlString.find('生活') != -1
	assert xmlString.find('運動戶外') != -1
	assert xmlString.find('美妝') != -1
	assert xmlString.find('衣鞋包錶') != -1
	os.system('sleep 1')

# 4. [Screenshot] Categories
def test_screenshotCategories():
	os.system('adb shell screencap -p /sdcard/screen.png && adb pull /sdcard/screen.png ./categories.png')
	os.system('sleep 1')

# 5. [Context] Categories page
def test_checkCategoriesPage():
	os.system('adb shell input tap 1000 250')
	os.system('sleep 1')
	os.system('adb shell input tap 250 1750')
	os.system('sleep 1')
	os.system('adb shell input swipe 200 1000 200 100')	
	os.system('sleep 1')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./categoriesPage.xml')
	f = open('./categoriesPage.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('通訊') != -1
	assert xmlString.find('數位') != -1
	assert xmlString.find('家電') != -1
	assert xmlString.find('日用') != -1
	assert xmlString.find('食品') != -1
	assert xmlString.find('生活') != -1
	assert xmlString.find('運動戶外') != -1
	assert xmlString.find('美妝') != -1
	assert xmlString.find('衣鞋包錶') != -1
	assert xmlString.find('書店') != -1
	assert xmlString.find('電子票券') != -1
	os.system('sleep 1')
	

# 6. [Screenshot] Categories page
def test_screenshotCategoriesPage():
	os.system('adb shell screencap -p /sdcard/screen.png && adb pull /sdcard/screen.png ./categoriesPage.png')
	os.system('sleep 1')

# 7. [Behavior] Search item “switch”
def test_searchItem():
	os.system('adb shell input tap 250 150')
	os.system('sleep 1')
	os.system('adb shell input text Switch')
	os.system('sleep 1')
	os.system('adb shell input keyevent 66')
	os.system('sleep 1')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./searchItem.xml')
	f = open('./searchItem.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('Switch') != -1
	os.system('sleep 1')

# 8. [Behavior] Follow an item and it should be add to the list
def test_followItem():
	os.system('adb shell input tap 500 500')
	os.system('sleep 2')
	os.system('adb shell input tap 100 1700')
	os.system('sleep 1')
	os.system('adb shell input tap 200 1900')
	os.system('sleep 1')
	os.system('adb shell input tap 200 1900')
	os.system('sleep 1')
	os.system('adb shell input tap 200 1900')
	os.system('sleep 1')
	os.system('adb shell input tap 100 100')
	os.system('sleep 2')
	os.system('adb shell input tap 300 900')
	os.system('sleep 1')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./followItem.xml')
	f = open('./followItem.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('Switch') != -1
	os.system('sleep 1')

# 9. [Behavior] Navigate tto the detail of item
def test_navigate():
	os.system('adb shell input tap 100 1000')
	os.system('sleep 3')
	os.system('adb shell input swipe 500 1000 500 100')	
	os.system('sleep 1')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./navigate.xml')
	f = open('./navigate.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('詳') != -1
	assert xmlString.find('情') != -1
	os.system('sleep 1')

# 10. [Screenshot] Disconnetion Screen
def test_screenshotDisconnection():
	os.system('adb shell svc data disable')
	os.system('sleep 1')
	os.system('adb shell svc wifi disable')
	os.system('sleep 1')
	os.system('adb shell screencap -p /sdcard/screen.png && adb pull /sdcard/screen.png ./disconnect.png')
	os.system('sleep 2')
	os.system('adb shell svc wifi enable')
	os.system('sleep 1')
	os.system('adb shell svc data enable')
	os.system('sleep 1')



